import { Injectable } from '@angular/core';
import {Router} from "@angular/router";


@Injectable()
export class AuthService {
    isLogged = false;

    constructor(private router: Router){}

    login(username: string, password: string) {
        this.isLogged = true;

        return username === 'admin' && password === 'admin';
    }

    logout() {
        this.router.navigate(['/login']);
        this.isLogged = false;
    }
}