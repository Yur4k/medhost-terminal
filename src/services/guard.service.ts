import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {AuthService} from "./auth.service";


@Injectable()
export class GuardService implements CanActivate{
    constructor(private authService: AuthService, private router: Router) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):  boolean {
        return true;

/*        if (this.authService.isLogged) {
            return true
        } else {
            this.router.navigate(['/login']);

            return false;
        }*/
    }
}