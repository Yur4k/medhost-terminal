import { Injectable } from '@angular/core';
import {StorageService} from "./storage.service";


@Injectable()
export class HelperService {
    constructor(private storageService: StorageService){}
    
    addVisite(userId, doctorId, day, time) {
        this.storageService.visitsStorage[doctorId].map((dayTime, _day) => {
            if (_day === day) {
                dayTime.map((visit) => {
                   if (visit["time"] === time) {
                       visit["user_id"] = userId;
                   }
                });
            }
        });
    }

    getDoctorById(id) {
        return this.storageService.doctorsStorage.find(doctor => doctor.id === id*1);
    }

    getUserById(id) {
        return this.storageService.pacientsStorage.find(pacient => pacient.id === id*1);
    }

    getPlanedVisites(userId) {
        let doctordb = this.storageService.visitsStorage;
        let visites = [];

        for (let doctor in doctordb) {
            doctordb[doctor].forEach((timeGrid, day) => {
                timeGrid.forEach((visite) => {
                   if (visite.user_id*1 === userId*1) {
                       visites.push({
                           doctor: this.getDoctorById(doctor),
                           day: day,
                           time: visite.time
                       });
                   }
                });
            });
        }

        return visites;
    }

    __generateVisiteGrid() {
        this.storageService.doctorsStorage.forEach((doctor) => {
            let maxDay = new Date().getDate();

            for (let day = maxDay; day < maxDay + 5; day++) {
                this.storageService.visitsStorage[doctor["id"]][day] = [];
                this.storageService.visitsStorage[doctor["id"]].map((timeGrid, _day) => {
                    if (_day === day) {
                        let hour = 9;
                        let minutes = -1;

                        while (hour !== 18) {
                            if (minutes === 0) {
                                hour++;
                            }

                            timeGrid.push({
                                time: `${hour !== 9 ? hour : "0"+hour}:${minutes !== -1 ? (minutes === 0 ? minutes+"0" : minutes) : "00"}`
                            });

                            if (minutes === 40) {
                                minutes = 0;
                            } else {
                                if (minutes === -1) {
                                    minutes = 20;
                                } else {
                                    minutes += 20;
                                }
                            }
                        }
                    }
                });
            }
        });
    }
}