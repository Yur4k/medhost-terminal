import { Injectable } from '@angular/core';
import {pacients} from "../data/pacients";
import {doctors} from "../data/doctors";
import {visits} from "../data/visits";

@Injectable()
export class StorageService {
    public pacientsStorage = pacients;
    public doctorsStorage = doctors;
    public visitsStorage = visits;
}