export const doctors = [
  {
    "id": 1124214,
    "username": "Сучко Юрий Иванович",
    "position": "Хирург"
  },
  {
    "id": 2423424,
    "username": "Кривченко Пётр Степанович",
    "position": "Терапевт"
  },
  {
    "id": 234,
    "username": "Азанов Кирилл Денов",
    "position": "Хирург"
  },
  {
    "id": 77458,
    "username": "Пикченко Андрей Сергеевич",
    "position": "Стоматолог"
  }
];