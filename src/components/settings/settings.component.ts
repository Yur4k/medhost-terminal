import { Component } from '@angular/core';


@Component({
    selector: 'settings',
    templateUrl: 'settings.component.html',
    styleUrls: ['settings.component.css'],
})
export class SettingsComponent {
    private isSubmited = false;
    private serverLoad = this.getData(1, 15);
    private serverPing = this.getData(30, 150);


    getData(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    submit(form) {
        if (this.isSubmited) {
            return;
        }
        this.isSubmited = true;
        form.value = "";
        alert('ваше сообщение отправлено!');
    }
}