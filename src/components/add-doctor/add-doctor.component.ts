import { Component } from '@angular/core';
import {StorageService} from "../../services/storage.service";
import {Router} from "@angular/router";


@Component({
    selector: 'add-doctor',
    templateUrl: 'add-doctor.component.html',
    styleUrls: ['add-doctor.component.css'],
})
export class AddDoctorComponent {
    constructor(private storageService: StorageService, private router: Router){}

    addDoctor(username: string, position: string) {
        this.storageService.doctorsStorage.push({
            id: Math.round(Math.random() * 10000000),
            username,
            position,
        });

        alert("Доктор успешно добавлен");
        this.router.navigate(['/doctors-list']);
    }
}