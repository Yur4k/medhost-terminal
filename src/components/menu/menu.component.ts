import { Component } from '@angular/core';
import {StorageService} from "../../services/storage.service";
import {HelperService} from "../../services/helper.service";


@Component({
    selector: 'menu',
    templateUrl: 'menu.component.html',
    styleUrls: ['menu.component.css'],
})
export class MenuComponent {
    constructor(private stroageService: StorageService, private helperService: HelperService){
        this.helperService.__generateVisiteGrid();
        console.log('Helper service is created.');
    }

    getStorageService() {
        console.log(this.stroageService.visitsStorage);
    }
}