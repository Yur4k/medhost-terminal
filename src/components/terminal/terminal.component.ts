import { Component } from '@angular/core';
import {AuthService} from "../../services/auth.service";


@Component({
    selector: 'terminal',
    templateUrl: 'terminal.component.html',
    styleUrls: ['terminal.component.css'],
})
export class TerminalComponent {
    constructor(private authService: AuthService){}

    logout() {
        this.authService.logout();
    }
}