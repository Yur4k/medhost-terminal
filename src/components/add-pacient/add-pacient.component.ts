import { Component } from '@angular/core';
import {StorageService} from "../../services/storage.service";
import {Router} from "@angular/router";


@Component({
    selector: 'add-pacient',
    templateUrl: 'add-pacient.component.html',
    styleUrls: ['add-pacient.component.css'],
})
export class AddPacientComponent {
    constructor(private storageService: StorageService, private router: Router){}

    addPacient(username: string, age: number) {
        this.storageService.pacientsStorage.push({
            id: Math.round(Math.random() * 10000000),
            username: username,
            age: age,
            isHide: false
        });

        alert("Пациент успешно добавлен");
        this.router.navigate(['/pacients']);
    }
}