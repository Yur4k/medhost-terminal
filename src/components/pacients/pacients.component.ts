import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {StorageService} from "../../services/storage.service";
import {Observable} from "rxjs/Observable";
import "rxjs/add/observable/fromEvent";
import "rxjs/add/operator/map";


@Component({
    selector: 'pacients',
    templateUrl: 'pacients.component.html',
    styleUrls: ['pacients.component.css'],
})
export class PacientsComponent implements AfterViewInit,OnInit{
    private pacients;
    @ViewChild("searchInput") searchInput;

    constructor(private storageService: StorageService){}

    ngOnInit() {
        this.pacients = this.storageService.pacientsStorage;
    }

    ngAfterViewInit() {
        this.pacients = this.storageService.pacientsStorage;
        Observable.fromEvent(this.searchInput.nativeElement, "input").map((e: any) => e.target).subscribe((text: any) => {
            this.pacients.map((pacient) => {
                pacient.isHide = pacient.username.indexOf(text.value) === -1;
            });
        });
    }
}