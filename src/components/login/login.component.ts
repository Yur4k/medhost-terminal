import { Component } from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";


@Component({
    selector: 'login',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.css'],
})
export class LoginComponent {
    constructor(private authService: AuthService, private router: Router) {}

    openCabinet(username: string, password: string) {
        if (this.authService.login(username, password)) {
            this.router.navigate(['/terminal']);
        } else {
            alert('Неверный логин/пароль');
        }
    }
}