import {Pipe, PipeTransform} from "@angular/core";


@Pipe({
    name: "sortVisites"
})
export class SortVisitesPipe implements PipeTransform {
    transform(visites) {
        visites.sort((visite1, visite2) => {
            if (visite1.day - visite2.day === 0) {
                return visite1.time < visite1.time2;
            }

            return visite1.day - visite2.day;
        });

        return visites;
    }

}