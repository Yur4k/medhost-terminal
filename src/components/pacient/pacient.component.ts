import {Component, OnInit} from '@angular/core';
import {HelperService} from "../../services/helper.service";
import {ActivatedRoute} from "@angular/router";
import {StorageService} from "../../services/storage.service";
import {isNullOrUndefined} from "util";


@Component({
    selector: 'pacient',
    templateUrl: 'pacient.component.html',
    styleUrls: ['pacient.component.css'],
})
export class PacientComponent implements OnInit{
    private user;
    private planedVisites;

    constructor(private helperService: HelperService, private route: ActivatedRoute, private storageService: StorageService){}

    ngOnInit() {
        this.planedVisites = this.helperService.getPlanedVisites(this.route.snapshot.params["id"]);
        this.user = this.helperService.getUserById(this.route.snapshot.params["id"]);
    }

    removeVisite(visite) {
        this.storageService.visitsStorage[visite.doctor.id].forEach((dayGrid, day) => {
            if (day === visite.day) {
                dayGrid.map((visiteInfo) => {
                    if (visiteInfo.time === visite.time) {
                        delete visiteInfo["user_id"];
                    }
                });
            }
        });
        this.planedVisites = this.helperService.getPlanedVisites(this.user.id);
    }
}