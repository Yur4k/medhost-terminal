import {Component, OnInit} from '@angular/core';
import {StorageService} from "../../services/storage.service";
import {ActivatedRoute, Router} from "@angular/router";


@Component({
    selector: 'doctors-list',
    templateUrl: 'doctors-list.component.html',
    styleUrls: ['doctors-list.component.css'],
})
export class DoctorsListComponent implements OnInit{
    private doctors;
    private userId;

    constructor(private storageService: StorageService, private route: ActivatedRoute, private router: Router){}

    ngOnInit() {
        this.doctors = this.storageService.doctorsStorage;
        this.route.queryParams.subscribe((params) => {
            this.userId = params["pin"];
        });
    }

    addVisite(doctorId) {
        this.router.navigate([`/doctor-visits/${doctorId}/${this.userId}`]);
    }
}