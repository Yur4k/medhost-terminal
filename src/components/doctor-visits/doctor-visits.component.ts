import {Component, OnInit} from '@angular/core';
import {StorageService} from "../../services/storage.service";
import {ActivatedRoute} from "@angular/router";
import {HelperService} from "../../services/helper.service";


@Component({
    selector: 'doctor-visits',
    templateUrl: 'doctor-visits.component.html',
    styleUrls: ['doctor-visits.component.css'],
})
export class DoctorVisitsComponent implements OnInit{
    private visitsGrid;
    private doctor;
    private user;

    constructor(private storageService: StorageService, private helperService: HelperService, private route: ActivatedRoute){}

    ngOnInit() {
        this.visitsGrid = this.storageService.visitsStorage[this.route.snapshot.params["doctor"]];
        this.doctor = this.helperService.getDoctorById(this.route.snapshot.params["doctor"]);
        this.user = this.helperService.getUserById(this.route.snapshot.params["user"]);
    }


    checkIfExpires(_day) {
        let day = new Date().getDate();

        if (_day < day) {
            return 'hidden-element';
        } else {
            return '';
        }
    }

    addVisite(day, time) {
        this.helperService.addVisite(this.user.id, this.doctor.id, day, time);
    }
}