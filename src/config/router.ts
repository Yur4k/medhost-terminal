import {Routes} from "@angular/router";
import {DoctorsListComponent} from "../components/doctors-list/doctors-list.component";
import {GuardService} from "../services/guard.service";
import {LoginComponent} from "../components/login/login.component";
import {TerminalComponent} from "../components/terminal/terminal.component";
import {PacientsComponent} from "../components/pacients/pacients.component";
import {AddPacientComponent} from "../components/add-pacient/add-pacient.component";
import {AddDoctorComponent} from "../components/add-doctor/add-doctor.component";
import {SettingsComponent} from "../components/settings/settings.component";
import {DoctorVisitsComponent} from "../components/doctor-visits/doctor-visits.component";
import {PacientComponent} from "../components/pacient/pacient.component";


export const appRoutes: Routes = [
    {
        path: "",
        pathMatch: "full",
        redirectTo: "terminal"
    },
    {
        path: "terminal",
        component: TerminalComponent,
        canActivate: [GuardService]
    },
    {
        path: "login",
        component: LoginComponent
    },
    {
        path: "doctors-list",
        component: DoctorsListComponent,
        canActivate: [GuardService]
    },
    {
        path: "pacients",
        component: PacientsComponent,
        canActivate: [GuardService]
    },
    {
        path: "add-pacient",
        component: AddPacientComponent,
        canActivate: [GuardService]
    },
    {
        path: "add-doctor",
        component: AddDoctorComponent,
        canActivate: [GuardService]
    },
    {
        path: "settings",
        component: SettingsComponent,
        canActivate: [GuardService]
    },
    {
        path: "doctor-visits/:doctor/:user",
        component: DoctorVisitsComponent,
        canActivate: [GuardService]
    },
    {
        path: "pacient/:id",
        component: PacientComponent,
        canActivate: [GuardService]
    },
];