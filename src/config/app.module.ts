import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import {AppComponent } from '../components/app/app.component';
import {appRoutes} from "./router";
import {RouterModule} from "@angular/router";
import {AuthService} from "../services/auth.service";
import {GuardService} from "../services/guard.service";
import {MenuComponent} from "../components/menu/menu.component";
import {LoginComponent} from "../components/login/login.component";
import {DoctorsListComponent} from "../components/doctors-list/doctors-list.component";
import {TerminalComponent} from "../components/terminal/terminal.component";
import {PacientsComponent} from "../components/pacients/pacients.component";
import {StorageService} from "../services/storage.service";
import {AddDoctorComponent} from "../components/add-doctor/add-doctor.component";
import {AddPacientComponent} from "../components/add-pacient/add-pacient.component";
import {SettingsComponent} from "../components/settings/settings.component";
import {DoctorVisitsComponent} from "../components/doctor-visits/doctor-visits.component";
import {HelperService} from "../services/helper.service";
import {PacientComponent} from "../components/pacient/pacient.component";
import {SortVisitesPipe} from "../components/pipes/sort-visites.pipe";


@NgModule({
  declarations: [
      AppComponent,
      MenuComponent,
      LoginComponent,
      DoctorsListComponent,
      TerminalComponent,
      PacientsComponent,
      AddDoctorComponent,
      AddPacientComponent,
      SettingsComponent,
      DoctorVisitsComponent,
      PacientComponent,
      SortVisitesPipe,
  ],
  imports: [
      BrowserModule,
      FormsModule,
      HttpModule,
      RouterModule.forRoot(appRoutes)
  ],
  providers: [AuthService, GuardService, StorageService, HelperService, SortVisitesPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
